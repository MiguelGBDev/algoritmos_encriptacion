
//Autores: Miguel Angel Gomez Baena y Pablo Moralo Flores


package bss1;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;

import javax.activation.FileDataSource;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class principal {

	String passwd;
	String algorithm;
	int iterationCount;
	PBEKeySpec pbeKeySpec;
	PBEParameterSpec pPS;
	SecretKeyFactory kf;
	SecretKey sKey;
	Cipher c;
	Cipher c2;
	Header head;
	String origen = "pac-man.jpg";
	String destino = "destino.txt";
	String destino2 = "pacman222.jpg";
	byte[] salt;

	public principal(String passwd, String algorithm) throws NoSuchAlgorithmException, InvalidKeySpecException{
		
		this.passwd = passwd;
		this.algorithm = algorithm;
		this.iterationCount = 8;
		
		salt = new byte[] { 0x7d, 0x60, 0x43, 0x5f, 0x02, (byte) 0xe9, (byte) 0xe0, (byte) 0xae };
		head = new Header(algorithm, salt);
		
		pbeKeySpec = new PBEKeySpec(passwd.toCharArray());
		pPS = new PBEParameterSpec(salt, iterationCount);
		
		kf = SecretKeyFactory.getInstance(algorithm);
		sKey = kf.generateSecret(pbeKeySpec);		
	}

	public principal(String passwd){
		
		salt = new byte[] { 0x7d, 0x60, 0x43, 0x5f, 0x02, (byte) 0xe9, (byte) 0xe0, (byte) 0xae };
		this.passwd = passwd;
		this.iterationCount = 8;
		pbeKeySpec = new PBEKeySpec(passwd.toCharArray());
		pPS = new PBEParameterSpec(salt, iterationCount);
		head = new Header();
	}
		
	public void Encriptar() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException{
					
		try {

			c = Cipher.getInstance(algorithm);
			c.init(Cipher.ENCRYPT_MODE, sKey, pPS);

			FileInputStream fis;
			FileOutputStream fos;
			CipherOutputStream cos1;
			
			fis = new FileInputStream(origen);
			fos = new FileOutputStream(destino);
			cos1 = new CipherOutputStream(fos, c);
			 			 
			head.save(fos);			

			byte[] b = new byte[8];
			int i = fis.read(b);
			while (i != -1) {
			    cos1.write(b, 0, i);
			    i = fis.read(b);
			}
			cos1.flush();
			cos1.close();
			fis.close();
			fos.close(); 
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Desencriptar()throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException{			 											
		
		try{
			FileInputStream fis2;
			FileOutputStream fos2;
			CipherOutputStream cos2;

			fis2 = new FileInputStream(destino);
			fos2 = new FileOutputStream(destino2);
			
			
			if (head.load(fis2)){		
			
			String algoritmo = head.getAlgorithm();
			
			kf = SecretKeyFactory.getInstance(algoritmo);
			sKey = kf.generateSecret(pbeKeySpec);
			
			
			c2 = Cipher.getInstance(algoritmo);
			c2.init(Cipher.DECRYPT_MODE, sKey, pPS);
			cos2 = new CipherOutputStream(fos2, c2);
			
			byte[] b = new byte[8];
			int i = fis2.read(b); 			
						
			 while (i != -1) {
			    cos2.write(b, 0, i);
			    i = fis2.read(b);
			 }
			
			cos2.flush(); 			 			 
			cos2.close();
			fis2.close();
			fos2.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String elegirAlgoritmo(Scanner sc){
		
		System.out.println("Seleccione el tipo de algoritmo de cifrado:");
		System.out.println("1. DES");
		System.out.println("2. 3DES");
		System.out.println("3. AES");
		int num = sc.nextInt();
		
		switch(num){
			case 1:	return "PBEWithMD5AndDES";
			case 2:	return "PBEWithMD5AndTripleDES1";					
			case 3:	return "PBEWithSHA1AndDESede";
			default:{ 
				System.out.println("la opcion no corresponde con ningun algoritmo, introduce una opcion correcta");
				return null;
			}
		}
	}
	
	public static String insertarPw(Scanner sc){
		
		System.out.println("Inserte la contrase�a: ");	
		
		return sc.next();
	}
	
	public static int elegirOpcion(Scanner sc){
		
		System.out.println("�Que desea?");
		System.out.println("1. Encriptar Archivo");
		System.out.println("2. Desencriptar Archivo");
		System.out.println("3. Salir");
		
		return sc.nextInt();
	}
	
	public String getDestino(){
		return destino;
	}
	
	public String getDestino2(){
		return destino2;
	}
	
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException  {

		String pass;
		String algoritmo = null;
		Scanner sc = new Scanner(System.in);

		int opcion = elegirOpcion(sc);		
					
		switch(opcion){	
			case 1:
				pass = insertarPw(sc);
				algoritmo =  elegirAlgoritmo(sc);
				principal p = new principal(pass, algoritmo);
				p.Encriptar();
				System.out.println("Encriptado realizado en fichero "+p.getDestino());
				break;
			case 2:
				pass = insertarPw(sc);
				principal q = new principal(pass);
				q.Desencriptar();
				System.out.println("Desencriptado realizado en fichero "+q.getDestino2());
				break;	
			case 3:
				break;
			default:
				System.out.println("la opcion elegida no es correcta");
			}		
		sc.close();				
	}
}
